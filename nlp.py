from flask import Flask, request, render_template, session, redirect, url_for, flash
from flask_bootstrap import Bootstrap
from flask_wtf import FlaskForm
from wtforms import TextAreaField, SubmitField
from wtforms.validators import DataRequired
from gensim.summarization import summarize

app = Flask(__name__)

app.config['SECRET_KEY'] = '#$34qwe'

boostrap = Bootstrap(app)

class SummaryForm(FlaskForm):
    post = TextAreaField('what is your post?', validators = [DataRequired()])
    submit = SubmitField('Submit')

@app.route('/nlp/', methods=['GET', 'POST'])
def nlp():
    summary = None 
    form = SummaryForm()
    if form.validate_on_submit():
        summary = summarize(form.post.data, split=True)
    return render_template('nlp.html', form=form, summary=summary)
